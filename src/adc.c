#include "adc.h"



LOG_MODULE_REGISTER(adc, LOG_LEVEL_INF);

int16_t buffer;
struct adc_sequence seq = {
        .buffer = &buffer,
        .buffer_size = sizeof(buffer), // bytes
};

int32_t readADC(const struct adc_dt_spec adc){
        int ret = 0;
        int32_t val_mv;
        (void)adc_sequence_init_dt(&adc, &seq);
        ret = adc_read(adc.dev, &seq);
        if (ret < 0) {
                LOG_ERR("Could not read (%d)", ret);
        }
        val_mv = buffer;
        ret = adc_raw_to_millivolts_dt(&adc, &val_mv);
        if (ret < 0) {
                LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
        }

        //LOG_INF("ADC Reading: %d", val_mv);
        return val_mv;
}
