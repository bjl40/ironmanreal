#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/devicetree.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/pwm.h>

#include "adc.h"

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

//define macros
#define HEARTBEAT_PERIOD_MS 500
#define SLEEP_TIME_MS   100
#define AVERAGE_COLLECTION_TIME_MS 400
#define FREQ_READ_kHZ 2
#define CALLIBRATION_TIME_MS 1000
#define CALLIBRATION_NUM_SIZE FREQ_READ_kHZ*CALLIBRATION_TIME_MS
#define TOTAL_NUM_VALUES AVERAGE_COLLECTION_TIME_MS*FREQ_READ_kHZ
#define SERVO_UPDATE_TIME_MS 20
#define MINSERVO 500
#define MAXSERVO 2500

#define READY_EVENT BIT(0)
#define DOWN_EVENT BIT(1)
#define SWITCH_EVENT BIT(2)
#define CALIBRATE_EVENT BIT(3)
#define COLLECT_EVENT BIT(4)


//define button aliases
#define MOTORUP    DT_ALIAS(motorup)
#define MOTORDOWN    DT_ALIAS(motordown)
#define SWITCHMODE DT_ALIAS(switchmode)
#define HEARTBEAT_LED   DT_ALIAS(heartbeat)
#define ERROR_LED       DT_ALIAS(errorl)
#define CALLIBRATEB     DT_ALIAS(callibrate)

#define ADC_DT_SPEC_GET_BY_ALIAS(node_id)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(node_id))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(node_id)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(node_id))          \
}

long callibration_total = 0;
int num_callibration = 0;
int raw_emg_values[TOTAL_NUM_VALUES] = {0};
int emg_pos = 0;
int sum_values = 0;
int average_values = 0;

int low_val = 22;
int high_val = 1550;
const struct pwm_dt_spec motor1 = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));
const struct pwm_dt_spec motor2 = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));

/*Define GPIO Structs*/
static const struct gpio_dt_spec motorUpButton = GPIO_DT_SPEC_GET(MOTORUP, gpios);
static const struct gpio_dt_spec motorDownButton = GPIO_DT_SPEC_GET(MOTORDOWN, gpios);
static const struct gpio_dt_spec switchModeButton = GPIO_DT_SPEC_GET(SWITCHMODE, gpios);
static const struct gpio_dt_spec callibrateButton = GPIO_DT_SPEC_GET(CALLIBRATEB, gpios);

static const struct gpio_dt_spec heartbeatLED = GPIO_DT_SPEC_GET(HEARTBEAT_LED, gpios);
static const struct gpio_dt_spec errorLED = GPIO_DT_SPEC_GET(ERROR_LED, gpios);

static const struct adc_dt_spec emg_adc = ADC_DT_SPEC_GET_BY_ALIAS(emgadc1);
/*GPIO CB Data*/


static struct gpio_callback ready_cb;
static struct gpio_callback down_cb;
static struct gpio_callback calibrate_cb;
static struct gpio_callback switch_cb;

void ready_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void down_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void calibrate_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void switch_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
//Callback Data

//define callback and timer functions
void emgTimerHandler();
void emgReadWork();
void heartbeatToggle();
void update_pwm();
void pwmUpdateTimerHandler();
void emg_pos_updater();
void postUserReady();
void callibrateCollect();
/*Timer/Work of GPIO interrupts*/
K_TIMER_DEFINE(heartbeatTimer, heartbeatToggle, NULL);
K_TIMER_DEFINE(pwmUpdateTimer, pwmUpdateTimerHandler, NULL);
K_WORK_DEFINE(updateLEDS, update_pwm);
K_WORK_DEFINE(emgPosUpdateWork, emg_pos_updater);

K_TIMER_DEFINE(emgTimer, emgTimerHandler, NULL);
K_WORK_DEFINE(emgWork, emgReadWork);

K_TIMER_DEFINE(collectionTimer, callibrateCollect, NULL);

/** State Machine Information*/
static const struct smf_state demo_states[];

enum demo_state {initS, callibrateS, manualS, autoS, errorS};
struct s_object {
        /* This must be first */
        struct smf_ctx ctx;
        int brightness;
        struct k_event smf_event;
        int32_t events;
} s_obj;
void callibrateCollect(){
        if (num_callibration < CALLIBRATION_NUM_SIZE){
                int val = readADC(emg_adc);
                if (val < 0){ val = -val;}
                callibration_total += val;
                num_callibration++;
                //LOG_INF("COLLECITNG");
        } else {
                LOG_INF("STOPPING");
                k_timer_stop(&collectionTimer);
                k_event_post(&s_obj.smf_event, COLLECT_EVENT);
                num_callibration = 0;
        }
}

void postUserReady(){
        k_event_post(&s_obj.smf_event, READY_EVENT);
}
void emgTimerHandler(){
        k_work_submit(&emgWork);
        k_work_submit(&emgPosUpdateWork);
}
void emg_pos_updater(){
        s_obj.brightness = 100*(average_values - low_val)/(high_val-low_val);
}
void emgReadWork(){
        if (emg_pos > TOTAL_NUM_VALUES){
                LOG_INF("EMG VALUE IS: %d", average_values);
                emg_pos = 0;
        }
        sum_values -= raw_emg_values[emg_pos];
        raw_emg_values[emg_pos] = readADC(emg_adc);
        if (raw_emg_values[emg_pos] < 0){
                raw_emg_values[emg_pos] = -raw_emg_values[emg_pos];
        }
        sum_values += raw_emg_values[emg_pos];
        average_values = sum_values/(TOTAL_NUM_VALUES);
        emg_pos++;
}
void update_pwm(){
       // LOG_INF("Position: %d", s_obj.brightness);
        int position = (MAXSERVO-MINSERVO)*s_obj.brightness/100 + MINSERVO;
        //LOG_INF("Motor Position: %d", position);
        
        pwm_set_pulse_dt(&motor1, motor1.period * s_obj.brightness/100);
        pwm_set_pulse_dt(&motor2, PWM_USEC(position));
}

void pwmUpdateTimerHandler(){
        k_work_submit(&updateLEDS);
}
void heartbeatToggle(){
        gpio_pin_toggle_dt(&heartbeatLED);
       //LOG_INF("Heartbeat Done (%lld)", diff);
}
static void init_entry(void *o){
        s_obj.brightness = 0;
        //Check both gpio processors
        if (!device_is_ready(motorUpButton.port)){
                LOG_ERR("gpio0 interface not ready.");
                //return -1;
        }
        //Configure all I/O pins
        int err;
        err = gpio_pin_configure_dt(&heartbeatLED, GPIO_OUTPUT_ACTIVE);
	if (err < 0) {
                LOG_ERR("heartbeatLED not configured.");
		//return -1;
	}
        err = gpio_pin_configure_dt(&errorLED, GPIO_OUTPUT_INACTIVE);
	if (err < 0) {
                LOG_ERR("errorLED not configured.");
		//return -1;
	}
        err = gpio_pin_configure_dt(&motorUpButton, GPIO_INPUT);
        if (err < 0) {
                LOG_ERR("Cannot configure motorUp pin.");
                //return err;
        }
        err = gpio_pin_configure_dt(&motorDownButton, GPIO_INPUT);
        if (err < 0) {
                LOG_ERR("Cannot configure motorDown pin.");
                //return err;
        }
        err = gpio_pin_configure_dt(&callibrateButton, GPIO_INPUT);
        if (err < 0) {
                LOG_ERR("Cannot configure motorDown pin.");
                //return err;
        }
        err = gpio_pin_configure_dt(&switchModeButton, GPIO_INPUT);
        if (err < 0) {
                LOG_ERR("Cannot configure motorDown pin.");
                //return err;
        }
        
        /* Configure the PWM channel */
        if (!device_is_ready(motor1.dev))  {
                LOG_ERR("PWM device %s is not ready.", motor1.dev->name);
                //return -1;
        }

        
        
        err = pwm_set_pulse_dt(&motor1, 0); // 0% duty cycle
        if (err) {
        LOG_ERR("Could not set motor1  driver 1 (PWM0)");
        }
        
        err = pwm_set_pulse_dt(&motor2, 0); // 0% duty cycle
        if (err) {
        LOG_ERR("Could not set motor2 driver 1 (PWM0)");
        }

        /* Configure ADC Channel*/
        if (!device_is_ready(emg_adc.dev)) {
                LOG_ERR("ADC controller device(s) not ready");
                //return -1;
        }
        
        err = adc_channel_setup_dt(&emg_adc);
        if (err < 0) {
                LOG_ERR("Could not setup ADC channel (%d)", err);
                //return err;
        }

        gpio_init_callback(&switch_cb, switch_button_handler, BIT(switchModeButton.pin));
        gpio_init_callback(&down_cb, down_button_handler, BIT(motorDownButton.pin));
        gpio_init_callback(&calibrate_cb, calibrate_button_handler, BIT(callibrateButton.pin));
        gpio_init_callback(&ready_cb, ready_button_handler, BIT(motorUpButton.pin));

        
        gpio_add_callback(switchModeButton.port, &switch_cb);
        gpio_add_callback(callibrateButton.port, &calibrate_cb);
        gpio_add_callback(motorDownButton.port, &down_cb);
        gpio_add_callback(motorUpButton.port, &ready_cb);
        k_timer_start(&heartbeatTimer, K_MSEC(HEARTBEAT_PERIOD_MS), K_MSEC(HEARTBEAT_PERIOD_MS));
        

        smf_set_state(SMF_CTX(&s_obj), &demo_states[callibrateS]);
}

static void callibrate_entry(void *o){
        LOG_INF("Callibration Initiated: Position arm at low state and press anything to begin");
        gpio_pin_interrupt_configure_dt(&motorUpButton, GPIO_INT_EDGE_TO_ACTIVE);
}
static void callibrate_run(void *o){
        k_timer_start(&collectionTimer, K_NO_WAIT, K_USEC(1000/FREQ_READ_kHZ));
        k_event_wait_all(&s_obj.smf_event, COLLECT_EVENT, true, K_FOREVER);
        low_val = callibration_total/(CALLIBRATION_NUM_SIZE);


        LOG_INF("Press Ready when at High State");
        k_event_wait_all(&s_obj.smf_event, READY_EVENT, true, K_FOREVER);
        callibration_total = 0;
        k_timer_start(&collectionTimer, K_NO_WAIT, K_USEC(1000/FREQ_READ_kHZ));
        k_event_wait_all(&s_obj.smf_event, COLLECT_EVENT, true, K_FOREVER);
        high_val = callibration_total/(CALLIBRATION_NUM_SIZE);
        callibration_total = 0;

        
        LOG_INF("Low val: %d, High val: %d", low_val, high_val);
        if (low_val < 0 || high_val > 3500 || low_val + 5 > high_val){
                LOG_INF("Callibration Error, Please Restart Device");
                smf_set_state(SMF_CTX(&s_obj), &demo_states[errorS]);
        } else {
                smf_set_state(SMF_CTX(&s_obj), &demo_states[autoS]);
        }
}
static void callibrate_exit(void *o){
        gpio_pin_interrupt_configure_dt(&motorUpButton, GPIO_INT_DISABLE);
}
static void auto_entry (void *o){
        gpio_pin_interrupt_configure_dt(&switchModeButton, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_pin_interrupt_configure_dt(&callibrateButton, GPIO_INT_EDGE_TO_ACTIVE);
        k_timer_start(&emgTimer, K_NO_WAIT, K_USEC(1000/FREQ_READ_kHZ));
        k_timer_start(&pwmUpdateTimer, K_MSEC(SERVO_UPDATE_TIME_MS), K_MSEC(SERVO_UPDATE_TIME_MS));
}

static void auto_run(void *o){
        struct s_object *s = (struct s_object *)o;
        if (s->events & CALIBRATE_EVENT){
                smf_set_state(SMF_CTX(&s_obj), &demo_states[callibrateS]);
        }

        if (s->events & SWITCH_EVENT){
                smf_set_state(SMF_CTX(&s_obj), &demo_states[manualS]);
        }
}

static void auto_exit (void *o){
        k_timer_stop(&emgTimer);
        k_timer_stop(&pwmUpdateTimer);
        gpio_pin_interrupt_configure_dt(&switchModeButton, GPIO_INT_DISABLE);
        gpio_pin_interrupt_configure_dt(&callibrateButton, GPIO_INT_DISABLE);
}

static void manual_entry(void *o){
        LOG_INF("ENTERING ACTION");
        gpio_pin_interrupt_configure_dt(&motorUpButton, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_pin_interrupt_configure_dt(&switchModeButton, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_pin_interrupt_configure_dt(&motorDownButton, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_pin_interrupt_configure_dt(&callibrateButton, GPIO_INT_EDGE_TO_ACTIVE);
        update_pwm();
        //LOG_INF("ENTERING ACTION");
}
static void manual_run(void *o){
        struct s_object *s = (struct s_object *)o;
        
        if (s->events & READY_EVENT){
                while(gpio_pin_get_dt(&motorUpButton) && s_obj.brightness < 100){
                        s_obj.brightness+=2;
                        update_pwm();
                        k_msleep(SLEEP_TIME_MS);
                }
        }
        if (s->events & DOWN_EVENT){
                while(gpio_pin_get_dt(&motorDownButton) && s_obj.brightness > 0){
                        s_obj.brightness-=2;
                        update_pwm();
                        k_msleep(SLEEP_TIME_MS);
                }
        }
        if (s->events & CALIBRATE_EVENT){
                smf_set_state(SMF_CTX(&s_obj), &demo_states[callibrateS]);
        }
        if (s->events & SWITCH_EVENT){
                smf_set_state(SMF_CTX(&s_obj), &demo_states[autoS]);
        }
        LOG_INF("RUNNING MANUAL %d", s_obj.brightness);
}

static void manual_exit(void *o){
        gpio_pin_interrupt_configure_dt(&motorUpButton, GPIO_INT_DISABLE);
        gpio_pin_interrupt_configure_dt(&motorDownButton, GPIO_INT_DISABLE);
        gpio_pin_interrupt_configure_dt(&switchModeButton, GPIO_INT_DISABLE);
        gpio_pin_interrupt_configure_dt(&callibrateButton, GPIO_INT_DISABLE);
        pwm_set_pulse_dt(&motor1, 0);
        pwm_set_pulse_dt(&motor2, 0);
        //LOG_INF("EXITING ACTOIN");
}

static void error_entry(void *o){
        gpio_pin_set_dt(&errorLED, 1);
}

static const struct smf_state demo_states[] = {
        [initS] = SMF_CREATE_STATE(init_entry, NULL, NULL),
        [callibrateS] = SMF_CREATE_STATE(callibrate_entry, callibrate_run, callibrate_exit),
        [manualS] = SMF_CREATE_STATE(manual_entry, manual_run, manual_exit),
        [autoS] = SMF_CREATE_STATE(auto_entry, auto_run, auto_exit),
        [errorS] = SMF_CREATE_STATE(error_entry, NULL, NULL)
};

//main
void main(void) {       
        k_event_init(&s_obj.smf_event);
        smf_set_initial(SMF_CTX(&s_obj), &demo_states[initS]);
        int32_t ret;
        while(1){
                s_obj.events = k_event_wait(&s_obj.smf_event,
                            READY_EVENT | DOWN_EVENT | CALIBRATE_EVENT | SWITCH_EVENT | COLLECT_EVENT, true, K_FOREVER);
                LOG_INF("RUNNING STATE MACHINE");
                ret = smf_run_state(SMF_CTX(&s_obj));
                if (ret) {
                        /* handle return code and terminate state machine */
                        break;
                }
                k_msleep(SLEEP_TIME_MS);
        }
}



void ready_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
        k_event_post(&s_obj.smf_event, READY_EVENT);
}
void down_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
        k_event_post(&s_obj.smf_event, DOWN_EVENT);
}
void calibrate_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
        k_event_post(&s_obj.smf_event, CALIBRATE_EVENT);
}
void switch_button_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
        k_event_post(&s_obj.smf_event, SWITCH_EVENT);
}