#ifndef ADC_H
#define ADC_H

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/logging/log.h>
#include <zephyr/drivers/adc.h>

int32_t readADC(const struct adc_dt_spec adc);

#endif